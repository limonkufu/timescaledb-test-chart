SHELL:=/bin/bash
PROJECT = timescaledb-test
KUBE_NAMESPACE ?= timescaledb-test
KUBE_HOST ?= `minikube ip`
RELEASE_NAME ?= test

MINIKUBE ?= true ## Minikube or not

# include k8s support
include .make/k8s.mk

# include Helm Chart support
include .make/helm.mk

# include core make support
include .make/base.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

CI_JOB_ID ?= local##pipeline job id
CLUSTER_DOMAIN ?= cluster.local## Domain used for naming Tango Device Servers

helm-add-repo:
	helm repo add timescale 'https://charts.timescale.com'

k8s-pre-install-chart: helm-add-repo

dev-up: k8s-namespace k8s-install-chart k8s-wait k8s-timescale-creds ## bring up developer deployment

dev-down: k8s-uninstall-chart k8s-delete-namespace  ## tear down developer deployment

k8s-timescale-creds:
	echo "Extracting information about current deployment";\
	echo "************************************************************************************"
	echo "****** VPN required ****************************************************************"
	echo "Pgadmin location: http://$pgadmin/"
	echo "Postgres location: $postgres"
	echo "************************************************************************************"
	echo "****** NO VPN required *************************************************************"
	echo "Pgadmin location: https://k8s.stfc.skao.int/${KUBE_NAMESPACE}/pgadmin4/browser/"
	echo "************************************************************************************"
	echo "kubectl run -i --tty --rm psql --image=postgres \
  	--env "PGPASSWORD=$$(kubectl get secret --namespace ${KUBE_NAMESPACE} ${RELEASE_NAME}-credentials -o jsonpath="{.data.PATRONI_admin_PASSWORD}" | base64 --decode)" \
  	--command -- psql -U admin \
  	-h ${RELEASE_NAME}.default.svc.cluster.local postgres"
	echo "PGPASSWORD_ADMIN=$$(kubectl get secret --namespace ${KUBE_NAMESPACE} ${RELEASE_NAME}-credentials -o jsonpath="{.data.PATRONI_admin_PASSWORD}" | base64 --decode)"
	echo "PGPASSWORD_POSTGRES=$$(kubectl get secret --namespace ${KUBE_NAMESPACE} ${RELEASE_NAME}-credentials -o jsonpath="{.data.PATRONI_SUPERUSER_PASSWORD}" | base64 --decode)"
	

.PHONY: $(MAKECMDGOALS) 